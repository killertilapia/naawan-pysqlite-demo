"""
For docs
@see https://docs.python.org/2/library/sqlite3.html#
"""

import sqlite3

# configs
DB = "demo.db"


def connect_db():
    return sqlite3.connect(DB)


def init_db():
    """
    Also resets the database
    To use, open a python shell session and just import and run it
    > from main import init_db
    > init_db()
    """
    with connect_db() as conn:
        print("Initializing/Resetting DB")
        
        with open('schema.sql', 'r') as f:
            conn.executescript(f.read())

    print(f"{DB} initialized")


def add_student_to_db(sid, lname, fname):
    """
    @see https://docs.python.org/2/library/sqlite3.html#using-the-connection-as-a-context-manager
    """
    with connect_db() as conn:
        conn.execute("INSERT INTO students(stud_id, last_name, first_name) VALUES (?, ?, ?)", (sid, lname, fname))


def update_student_in_db(sid, lname, fname):
    with connect_db() as conn:
        conn.execute("UPDATE students SET last_name = ?, first_name = ? WHERE stud_id = ? ", (sid, lname, fname))


def find_student_in_db(sid):
    pass


def delete_student_in_db(sid):
    pass


def start():
    add_student_to_db("22501", "Santos", "Rose")


if __name__ == '__main__':
    start()