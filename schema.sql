drop table if exists students;

create table students (
    id integer primary key autoincrement,
    stud_id text not null,
    last_name text not null,
    first_name text not null
);

INSERT INTO students(stud_id, last_name, first_name) VALUES ('13040', "Dela Cruz", "Juan");
INSERT INTO students(stud_id, last_name, first_name) VALUES ('11202', "Clara", "Maria");
INSERT INTO students(stud_id, last_name, first_name) VALUES ('22113', "Jones", "James");